"""Clase padre"""
class Precio():
    """Constructor"""
    def __init__(self, p):
        self.precio = p

    """Método para calcular el IVA del producto"""
    def calculo_iva(self):
        iva = self.precio * 0.21
        return iva   

class Factura(Precio):
    """Construstor de la clase hija"""
    def __init__(self, p, e, c):
        super().__init__(p)
        self.emisor = e
        self.cliente = c

    """Método de impresión de la factura"""
    def imprimirFactura(self):
        mensaje = """
        Factura
        Realizada por: {emisor}
        Cliente: {cliente}
        Importe: {precio}
        IVA: {iva}
        """.format(emisor=self.emisor, cliente=self.cliente, 
        precio=self.precio, iva = Precio.calculo_iva(self))
        return mensaje
