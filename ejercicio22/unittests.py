import unittest
from precio import Precio
from precio import Factura

class TestPrecio(unittest.TestCase):
    """Prueba de almacenamiento"""
    def test_init01(self):
        product = Precio(200)
        self.assertEqual(product.precio, 200)

    """Prueba del calculo del IVA"""
    def test_calculo_iva(self):
        product = Precio(200)
        self.assertEqual(product.calculo_iva, 42)

class TestFactura(unittest.TestCase):
    """Prueba de almacenamiento 1"""
    def test_init01(self):
        product = Factura(200, "Alf", "Bet")
        self.assertEqual(product.emisor, "Alf")

    """Prueba de almacenamiento 2"""
    def test_init02(self):
        product = Factura(200, "Alf", "Bet")
        self.assertEqual(product.cliente, "Bet")

    """Prueba de impresion"""
    def test_imprimirFactura(self):
        product = Factura(200, "Alf", "Bet")
        self.assertEqual(product.imprimirFactura(), """
        Factura
        Realizada por: Alf
        Cliente: Bet
        Importe: 200
        IVA: 42
        """)
