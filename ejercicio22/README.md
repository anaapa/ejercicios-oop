# Ejercicios 22

## Enunciado
Construir una clase Factura que descienda de la clase Precio y que incluya dos atributos específicos llamados emisor y cliente y, al menos, un método llamado imprimirFactura.
## Procedimiento
Para la realización de este ejercicio, se ha realizado la clase Precio, la cual se construía con un solo atributo, el precio del producto. También incluye un método para calcular el IVA de ese mismo producto para poder consultarlo si se requiere. 
La clase hija, Factura, hace uso del polimorfismo y aumenta su construcción con un emisor de la misma, y el cliente. Incluye un método de impresión de la factura. 
